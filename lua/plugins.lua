require('packer').startup(function()

	--packer manages itself
	use 'wbthomason/packer.nvim'
	--Vim-Plug for Non-lua plugins
	use 'junegunn/vim-plug'


	--lsp Configuration
	use 'neovim/nvim-lspconfig'
	--Automatic LSP-Server Installation
	use 'kabouzeid/nvim-lspinstall'

	--Pretty Diagnostics etc.
	use {"folke/trouble.nvim",
		config = function()
	    	require("plugins.trouble-config")
	end
	}
	--Colorschemes with Highlightgroups fro LSP
	use 'folke/lsp-colors.nvim'

	--Material Colorscheme
	use 'marko-cerovac/material.nvim'
		require ('plugins/material-colorscheme')

	--Autocompletion
	use 'hrsh7th/nvim-compe'
	--Snippets
        use 'SirVer/ultisnips'
		require ('plugins/ultisnips-config')

	--Which-Key
	use {
  		"folke/which-key.nvim",
  			config = function()
    			require("plugins.which-key-config")
  			end
	}

	-- Icons
	use { "kyazdani42/nvim-web-devicons" }

	----Statusline--
	use  'hoob3rt/lualine.nvim'
	--use {"glepnir/galaxyline.nvim",
    		--config = function()
      		--require ('plugins/galaxyline-config1')
    	--end,
  	--}

	use {'lewis6991/gitsigns.nvim',
  		requires = {'nvim-lua/plenary.nvim'},
		config = function()
      			require("plugins.gitsigns").setup()
    		end,
    		event = "BufRead",
	}

	--Movement--

	--Change Surroundings
	use 'tpope/vim-surround'
	--Makes plugin mappings repeatable with "." See http://vimcasts.org/episodes/creating-repeatable-mappings-with-repeat-vim/ on how the plugin actually works.
	use  'tpope/vim-repeat'
	--Reworks the clipboard, see https://github.com/svermeulen/vim-easyclip for more info.
--	use 'svermeulen/vim-easyclip'
	--Automatically comment
	use 'tpope/vim-commentary'

	--Colorpreviews in nvim
	--use 'RRethy/vim-hexokinase' , { 'do': 'make hexokinase' }

	--VimWiki
	use  'vimwiki/vimwiki' 
end)



--USAGE--

-- You must run this or `PackerSync` whenever you make changes to your plugin configuration
-- :PackerCompile

-- Only install missing plugins
-- :PackerInstall

-- Update and install plugins
-- :PackerUpdate

-- Remove any disabled or unused plugins
-- :PackerClean

-- Performs `PackerClean` and then `PackerUpdate`
-- :PackerSync



