--General Config

require('plugins')
require('opts')
require('keys')

--Plugin Configs
require('plugins/compe-config')
-- require "core.status_colors"
-- require("plugins.gitsigns").config()
-- require("core.compe").config()
-- require("core.dashboard").config()
-- require("core.dap").config()
-- require("core.floatterm").config()
-- require("core.zen").config()
-- require("core.telescope").config()
-- require("core.treesitter").config()
-- require('plugins.which-key').config()

require ('plugins/lualine-config')




--LSP

require ('plugins/lspconfig')
require('ftplugin/lua-ls')
require('ftplugin/sh')
