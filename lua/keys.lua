--KEYBINDINGS--


-- Map leader to space
vim.g.mapleader = ' '

--Defining options to add to every keybinding
local opts = {noremap = true, silent = true} 

----Copy and Paste in Vim
--vim.api.nvim_set_keymap('', '<C-c>', '"+y', opts)
--vim.api.nvim_set_keymap('', '<C-x>', '"+c', opts)
--vim.api.nvim_set_keymap('', '<C-v>', '"+p', opts)

--LSP - Code Navigation

--Go to definition
vim.api.nvim_set_keymap('n', 'gd', [[<Cmd>lua vim.lsp.buf.definition()<CR>]], opts)
--Go to declaration
vim.api.nvim_set_keymap('n', 'gD', [[<Cmd>lua vim.lsp.buf.declaration()<CR>]], opts)
--Go to reference
vim.api.nvim_set_keymap('n', 'gr', [[<Cmd>lua vim.lsp.buf.references()<CR>]], opts)
--Go to implementation
vim.api.nvim_set_keymap('n', 'gi', [[<Cmd>lua vim.lsp.buf.implementation()<CR>]], opts)
--Hover
vim.api.nvim_set_keymap('n', 'K', [[<Cmd>lua vim.lsp.buf.hover()<CR>]], opts)
vim.api.nvim_set_keymap('n', '<C-k>', [[<Cmd>lua vim.lsp.buf.signature_help()<CR>]], opts)
--Go to next/previous diagnostics
vim.api.nvim_set_keymap('n', '<C-n>', [[<Cmd>lua vim.lsp.diagnostic.goto_next()<CR>]], opts)
vim.api.nvim_set_keymap('n', '<C-N>', [[<Cmd>lua vim.lsp.diagnostic.goto_prev()<CR>]], opts)

