


----LSP CONFIG---
--
--This LSP config uses the native Nvim LSP in conjunction with compe for autocompletion. It was installed with LspInstall.
--Additionally, linting is provided by the efm server, also installed with LspInstall. The EFM-Server provides two linting tools, shellcheck and shfmt, so far I've only tried shellcheck.
--EFM Server: https://github.com/mattn/efm-langserver
--Shellcheck: https://github.com/koalaman/shellcheck
--

--copied from Chris@machine: https://github.com/ChristianChiarulli/LunarVim/blob/5e2a0df7ef8752b2af38a670fdf89ef48e4daa0f/ftplugin/sh.lu

require("lspconfig").bashls.setup {
  cmd = {"/home/fm/.local/share/nvim/lspinstall/bash/node_modules/.bin/bash-language-server", "start" },
--  on_attach = require("lsp").common_on_attach,
  root_dir = vim.loop.os_homedir,
  filetypes = { "sh", "zsh" },
}

-- EFMCONFIG----
local sh_arguments = {}

local shfmt = { formatCommand = "shfmt -ci -s -bn", formatStdin = true }

local shellcheck = {
  LintCommand = "shellcheck -f gcc -x",
  lintFormats = { "%f:%l:%c: %trror: %m", "%f:%l:%c: %tarning: %m", "%f:%l:%c: %tote: %m" },
}

--Adapted from Chris@machine's global variables: https://github.com/ChristianChiarulli/LunarVim/blob/5e2a0df7ef8752b2af38a670fdf89ef48e4daa0f/lua/default-config.lua


local sh = {
      -- @usage can be 'shellcheck'
      linter = "shellcheck",
      -- @usage can be 'shfmt'
      diagnostics = {
        virtual_text = { spacing = 0, prefix = "" },
        signs = true,
        underline = true,
      },
}

if sh.linter == "shellcheck" then
  table.insert(sh_arguments, shellcheck)
end

require("lspconfig").efm.setup {
  -- init_options = {initializationOptions},
  cmd = { "/home/fm/.local/share/nvim/lspinstall/efm/efm-langserver" },
  init_options = { documentFormatting = true, codeAction = false },
  filetypes = { "sh" },
  settings = {
    rootMarkers = { ".git/" },
    languages = {
      sh = sh_arguments,
    },
  },
}
