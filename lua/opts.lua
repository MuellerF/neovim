--Automatically update and compile Packer(=pluginmanager) when plugins.lua is changed
vim.cmd('autocmd BufWritePost plugins.lua PackerCompile')

--Configfile for Ultisnips

--Autocompletion setup required for compe
vim.o.completeopt = "menuone,noselect"

--set colors for Hexokinoase
vim.o.termguicolors = true

vim.opt.clipboard = 'unnamedplus' -- allows neovim to access the system clipboard


