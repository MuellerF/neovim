
require('lualine').setup{
    options = {
        theme = 'material-nvim',
	component_separators = "",
	section_separators = "",
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = { {'branch'}, {'diff'} },
        lualine_c = {
		{'filename'},
		{'diagnostics', sources = {'nvim_lsp'}},
		{ function()
			local msg = 'No Active Lsp'
    			local buf_ft = vim.api.nvim_buf_get_option(0, 'filetype')
    			local clients = vim.lsp.get_active_clients()
    			if next(clients) == nil then return msg end
    			for _, client in ipairs(clients) do
      				local filetypes = client.config.filetypes
      			if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
        			return client.name
      			end
    			end
    			 return msg
  			end,
  			icon = '   LSP:'
			},
		},
        lualine_x = {'encoding', 'fileformat', 'filetype'},
        lualine_y = {'progress'},
        lualine_z = {'location'}
    }
}



-- local conditions = {
--   buffer_not_empty = function() return vim.fn.empty(vim.fn.expand('%:t')) ~= 1 end,
--   hide_in_width = function() return vim.fn.winwidth(0) > 80 end,
--   check_git_workspace = function()
--     local filepath = vim.fn.expand('%:p:h')
--     local gitdir = vim.fn.finddir('.git', filepath .. ';')
--     return gitdir and #gitdir > 0 and #gitdir < #filepath
--   end
-- }


-- -- Inserts a component in lualine_c at left section
-- local function ins_left(component)
--   table.insert(config.sections.lualine_c, component)
-- end

-- -- Inserts a component in lualine_x ot right section
-- local function ins_right(component)
--   table.insert(config.sections.lualine_x, component)
-- end














