--https://github.com/marko-cerovac/material.nvim

--Initialization
require('material').set()

--Setting theme (Options: darker, lighter, oceanic, palenight, deep ocean)
vim.g.material_style = "oceanic"

vim.g.material_italic_comments = true
vim.g.material_italic_keywords = true
vim.g.material_italic_functions = true
vim.g.material_italic_variables = false
vim.g.material_contrast = true
--Enable the border between verticaly split windows visable
vim.g.material_borders = false
--Disable background to use the terminals background
vim.g.material_disable_background = false
--Hide end of buffer lines ~
vim.g.material_hide_eob= true

--vim.g.material_custom_colors = { bg = "#26323880" }
--vim.g.material_custom_colors = { black = "#000000", bg = "#0F111A" }

vim.api.nvim_set_keymap('n', '<leader>-m', [[<Cmd>lua require('material.functions').toggle_style()<CR>]], { noremap = true, silent = true })
