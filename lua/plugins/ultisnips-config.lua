--Configfile for Ultisnips


-- vim.g.UltiSnipsExpandTrigger = "<leader>-tab"
vim.g.UltiSnipsJumpForwardTrigger = "c-b"
vim.g.UltiSnipsJumpBackwardTrigger = "c-z"

--Open UltiSnipsEdit in a vertical Split
vim.g.UltiSnipsEditSplit = "vertical"

