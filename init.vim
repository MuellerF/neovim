
"Allow execution of lua 
lua require 'init'

call plug#begin('~/.local/share/nvim/plugged')

Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

call plug#end()

"Basics
	set nocompatible
	syntax on
	filetype plugin on
"Show relative line numbers
	set number relativenumber

"Display an incomplete command in the lower right corner
	set showcmd

"Display matches for search patterns
	set incsearch

"Enable autocompletion
	set wildmode=longest,list,full

"Automatically update and compile Packer(=pluginmanager) when plugins.lua is changed
"autocmd BufWritePost plugins.lua PackerCompile

"Autocommand to automatically format a lua file on write
"autocmd BufWritePre *.lua lua vim.lsp.buf.formatting_sync(nil, 100)

"Enable Systemclipboard
" set clipboard+=unnamedplus,unnamed

