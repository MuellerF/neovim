--Which-key can be configured by giving arguments wk.setup table. Default options are found at https://github.com/folke/which-key.nvim
--Keybindings can be configured and organized with the wk.register function. It takes mappings as a first argument, and options as a second.
--Here, mappings and opts are defined in two variants for normal and visual mode. Both tables are then passed as arguments to wk.register.
--Mappings are organized in the following format:


local wk = require("which-key")

vim.o.timeoutlen = 300

--local presets = require("which-key.plugins.presets")
--presets.objects["a("] = nil
wk.setup({
	triggers = "auto",
	plugins = { spelling = true },
	presets = {
		operators = true, -- adds help for operators like d, y, ... and registers them for motion / text object completion
    		motions = true, -- adds help for motions
    		text_objects = true, -- help for text objects triggered after entering an operator
    		windows = true, -- default bindings on <c-w>
    		nav = true, -- misc bindings to work with windows
    		z = true, -- bindings for folds, spelling and others prefixed with z
    		g = true, -- bindings for prefixed with g
	},
    	key_labels = { ["<leader>"] = "SPC" },
	triggers_blacklist = {
    		i = { "j", "k" },
    		v = { "j", "k" },
	}
})

--Mappings for normal mode
local normalmappings = {

	["w"] = { "<cmd>w!<CR>", "Save" },
	["q"] = { "<cmd>q!<CR>", "Quit" },
	["c"] = { "<cmd>BufferClose<CR>", "Close Buffer" },
	-- ["e"] = { "<cmd>lua require'core.nvimtree'.toggle_tree()<CR>", "Explorer" },
	-- ["f"] = { "<cmd>Telescope find_files<CR>", "Find File" },
	["h"] = { '<cmd>let @/=""<CR>', "No Highlight" },
	["m"] = { "<cmd>lua require('material.functions').toggle_style()<CR>", "Change Colorscheme" },
	p = {
		name = "Packer",
		c = { "<cmd>PackerCompile<cr>", "Compile" },
       		i = { "<cmd>PackerInstall<cr>", "Install" },
       		s = { "<cmd>PackerSync<cr>", "Sync" },
       		u = { "<cmd>PackerUpdate<cr>", "Update" },
       		S = { "<cmd>PackerStatus<cr>", "Status" },
	},
	x = {
		name = "Errors",
		x = { "<cmd>TroubleToggle<cr>", "Trouble" },
		w = { "<cmd>TroubleWorkspaceToggle<cr>", "Workspace Trouble" },
		d = { "<cmd>TroubleDocumentToggle<cr>", "Document Trouble" },
		l = { "<cmd>lopen<cr>", "Open Location List" },
		q = { "<cmd>copen<cr>", "Open Quickfix List" },
	},
	l = {
		name = "LSP",
		a = { "<cmd>lua vim.lsp.buf.code_action()<cr>", "Code Action" },
		-- d = { "<cmd>Telescope lsp_document_diagnostics<cr>", "Document Diagnostics" },
		-- w = { "<cmd>Telescope lsp_workspace_diagnostics<cr>", "Workspace Diagnostics" },
		f = { "<cmd>silent FormatWrite<cr>", "Format" },
		i = { "<cmd>LspInfo<cr>", "Info" },
		j = { "<cmd>lua vim.lsp.diagnostic.goto_next({popup_opts = {border = O.lsp.popup_border}})<cr>", "Next Diagnostic" },
		k = { "<cmd>lua vim.lsp.diagnostic.goto_prev({popup_opts = {border = O.lsp.popup_border}})<cr>", "Prev Diagnostic" },
		--q = { "<cmd>Telescope quickfix<cr>", "Quickfix" },
		r = { "<cmd>lua vim.lsp.buf.rename()<cr>", "Rename" },
		--s = { "<cmd> Telescope lsp_document_symbols<cr>", "Document Symbols" },
		-- S = { "<cmd>Telescope lsp_dynamic_workspace_symbols<cr>", "Workspace Symbols"},
	      },

	-- s = {
	--        name = "Search",
	--        b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
	--        c = { "<cmd>Telescope colorscheme<cr>", "Colorscheme" },
	--        f = { "<cmd>Telescope find_files<cr>", "Find File" },
	--        h = { "<cmd>Telescope help_tags<cr>", "Find Help" },
	--        M = { "<cmd>Telescope man_pages<cr>", "Man Pages" },
	--        r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
	--        R = { "<cmd>Telescope registers<cr>", "Registers" },
	--        t = { "<cmd>Telescope live_grep<cr>", "Text" },
	--        k = { "<cmd>Telescope keymaps<cr>", "Keymaps" },
	--        C = { "<cmd>Telescope commands<cr>", "Commands" },
	--      },

	-- T = {
	--        name = "Treesitter",
	--        i = { ":TSConfigInfo<cr>", "Info" },
      -- },

	-- g = {
	--         name = "Git",
	--         j = { "<cmd>lua require 'gitsigns'.next_hunk()<cr>", "Next Hunk" },
	--         k = { "<cmd>lua require 'gitsigns'.prev_hunk()<cr>", "Prev Hunk" },
	--         l = { "<cmd>lua require 'gitsigns'.blame_line()<cr>", "Blame" },
	--         p = { "<cmd>lua require 'gitsigns'.preview_hunk()<cr>", "Preview Hunk" },
	--         r = { "<cmd>lua require 'gitsigns'.reset_hunk()<cr>", "Reset Hunk" },
	--         R = { "<cmd>lua require 'gitsigns'.reset_buffer()<cr>", "Reset Buffer" },
	--         s = { "<cmd>lua require 'gitsigns'.stage_hunk()<cr>", "Stage Hunk" },
	--         u = {
	--           "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>",
	--           "Undo Stage Hunk",
	--         },

}

-- Mappings for visual mode
local visualmappings = {
	["/"] = { "<cmd>CommentToggle<CR>", "Comment" }, --Not working
}
--Options for normal mode
--local normalopts ={
--      mode = "n", -- NORMAL mode
--      prefix = "<leader>",
--      buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
--      silent = true, -- use `silent` when creating keymaps
--      noremap = true, -- use `noremap` when creating keymaps
--      nowait = true, -- use `nowait` when creating keymaps
--    },

----options for visual mode
--local visualopts = {
--      mode = "v", -- VISUAL mode
--      prefix = "<leader>",
--      buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
--      silent = true, -- use `silent` when creating keymaps
--      noremap = true, -- use `noremap` when creating keymaps
--      nowait = true, -- use `nowait` when creating keymaps
--    },

wk.register(normalmappings, { prefix = "<leader>" })
wk.register(visualmappings, { mode ="v", prefix = "<leader>" })
